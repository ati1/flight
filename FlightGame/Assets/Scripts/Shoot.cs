﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Shoot : MonoBehaviour {

	public GameObject ammoPrefab;
	public bool canShoot = true;

	public int ammoCase = 0;
	public int maxAmmoCase = 0;
	
	public bool rdyToShoot = false;
	public float shootDelay = 0.2f;
	public float reloadtime = 0f;
	
	private bool m_reloading = false;
	private float m_shootTimer = 0;

	private PlaneMovement shooter;
	private PlayerController m_player;
	private PlayGamesMultiplayer m_controller;

	private List<byte> m_shootMessage = new List<byte>();

	private AudioSource m_shootSound;

	public float shootPosDist;

	void Awake()
	{
		shooter = GetComponent<PlaneMovement> ();
		if(shooter != null)
			m_player = shooter.GetComponent<PlayerController>();
		m_controller = FindObjectOfType<PlayGamesMultiplayer> ();

		m_shootSound = transform.GetChild (2).GetComponent<AudioSource>();
	}

	public void Update()
	{
		if(canShoot)
			Fire();
	}
	
	public void Fire()
	{
		#if UNITY_STANDALONE_WIN
		if(rdyToShoot && Input.GetKey(KeyCode.Mouse0) && ammoCase > 0)
		#elif UNITY_ANDROID
		if(rdyToShoot && Input.touchCount > 0 && Input.GetTouch(0).phase != TouchPhase.Ended && Input.GetTouch(0).phase != TouchPhase.Canceled && ammoCase > 0)
		#endif
		{
			rdyToShoot = false;
			m_shootTimer = shootDelay;

			if(m_controller != null)
			{
				if(m_player.myParticipantId == m_controller.GetMyId())
				{
					Vector3 shootVector = transform.position + transform.forward * shootPosDist;
					Quaternion shootRot = transform.rotation;
					MultiplayerFire(shootVector.x,shootVector.y,shootVector.z,shootRot.x,shootRot.y,shootRot.z,shootRot.w);
					ShootAmmo(shootVector,shootRot);
				}
				//networkView.RPC("RPCFire",RPCMode.All,gameObject.transform.position + transform.forward * 5,transform.rotation);
			}
			else
			{
				ShootAmmo(transform.position + transform.forward * shootPosDist,transform.rotation);
			}

			
			if(ammoCase < 0)
				ammoCase = 0;
			
			ammoCase -= 1;
		}
		if(ammoCase == 0)
		{
			Reload();
		}
		
		if(!rdyToShoot && m_shootTimer < 0f)
			rdyToShoot = true;
		
		m_shootTimer--;
		
	}

	public void ShootAmmo(Vector3 pos, Quaternion rot)
	{
		GameObject temp = GameObject.Instantiate(ammoPrefab, pos, rot) as GameObject;
		temp.rigidbody.AddForce(transform.forward * (250 + shooter.speed),ForceMode.Impulse);
		m_shootSound.Play ();
	}

	public void MultiplayerFire(float posX, float posY, float posZ, float rotX, float rotY, float rotZ, float rotW)
	{
		byte m_protocolVersion = 1;
		m_shootMessage.Clear ();
		m_shootMessage.Add (m_protocolVersion);
		m_shootMessage.Add ((byte)'F');
		
		m_shootMessage.AddRange (System.BitConverter.GetBytes (posX));
		m_shootMessage.AddRange (System.BitConverter.GetBytes (posY));  
		m_shootMessage.AddRange (System.BitConverter.GetBytes (posZ));
		m_shootMessage.AddRange (System.BitConverter.GetBytes (rotX));
		m_shootMessage.AddRange (System.BitConverter.GetBytes (rotY));
		m_shootMessage.AddRange (System.BitConverter.GetBytes (rotZ));
		m_shootMessage.AddRange (System.BitConverter.GetBytes (rotW));
		
		byte[] messageToSend = m_shootMessage.ToArray();
		m_controller.SendMessageToEveryone(false, messageToSend);
	}
	
	public void Reload()
	{
		if(!m_reloading)
		{
			m_reloading = true;
			StartCoroutine("ReloadTimer");
		}
	}
	IEnumerator ReloadTimer()
	{
		yield return new WaitForSeconds(reloadtime);
		ammoCase = maxAmmoCase;
		m_reloading = false;
	}
	
//	void OnGUI()
//	{
//		if(networkView.isMine)
//		{
//			GUI.color = Color.black;
//			GUI.Label(new Rect(20,200,150,35),"Ammo:" + ammoCase +"/"+maxAmmoCase);
//		}	
//	}
}