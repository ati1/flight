﻿using UnityEngine;
using System.Collections;

public class ColliderScript : MonoBehaviour {

	public enum TriggerType
	{
		destroy,
		damage,
	};
	
	public float damage;
	
	public TriggerType triggerType;

	private PlayGamesMultiplayer m_manager;

	void Awake()
	{
		m_manager = FindObjectOfType<PlayGamesMultiplayer> ();
	}
	
	void OnTriggerEnter(Collider col)
	{
		if(triggerType == TriggerType.destroy)
		{
			if(col.GetComponent<PlayerController>())
			{
				Health temp = col.GetComponent<Health>();
				temp.Die ();
				
				if(m_manager != null)
					temp.MultiplayerDie();
				// tell others that we died
			}
		}
		else if(triggerType == TriggerType.damage)
		{
			PlayerController player = col.GetComponent<PlayerController>();

			// we are in multiplayer and hit some players vehicle and it is not ours
			if(m_manager != null && player != null)
			{
				//col.networkView.RPC("AdjustHealth",RPCMode.AllBuffered,-damage);
				Health pHealth = player.GetComponent<Health>();
				pHealth.AdjustMultiplayerHealth(player.myParticipantId , damage);

				//TODO: Wait for sync before doing this
				pHealth.AdjustHealth(-damage);
			}

			Destroy(gameObject);
		}
	}
}