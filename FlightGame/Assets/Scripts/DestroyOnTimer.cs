﻿using UnityEngine;
using System.Collections;

public class DestroyOnTimer : MonoBehaviour {

	public bool playOnAwake;
	[HideInInspector]
	public bool start;
	public float time;
	
	private float m_maxTime;
	
	void Awake()
	{
		m_maxTime = time;
		if(playOnAwake)
			start = true;
	}

	void Update()
	{
		if(start)
			Timer();
	}
	
	void Timer()
	{
		time -= Time.deltaTime;
		if(time <= 0)
		{
			time = m_maxTime;
			Destroy(gameObject);
		}
	}
}