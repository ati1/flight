﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[RequireComponent(typeof(NetworkView))]
public class PlayerNetworkValues : MonoBehaviour {
	
	public GameObject playerCamera;
	
	public int playerID;
	public string playerName;

	//Player movement variables
	private Transform m_transform;
	private Rigidbody m_rigidbody;
	private float m_syncTime; 
	private float m_syncDelay;
	private float m_lastSynchronizationTime;
	private Vector2 m_syncStartPosition;
	private Vector2 m_syncEndPosition;
	
	private Quaternion m_rotation = Quaternion.identity;
	[HideInInspector]
	public MultiplayerManager m_manager;
	private bool setUp;
	
	void Awake()
	{
		if(Network.isClient || Network.isServer)
		{
			m_manager = FindObjectOfType<MultiplayerManager>();
			m_transform = transform;
			m_rigidbody = rigidbody;
			
			m_lastSynchronizationTime = Time.time;
			playerCamera = GameObject.Instantiate(playerCamera) as GameObject;
			
			if(networkView.isMine)
			{
				playerCamera.gameObject.SetActive(true);
				GetComponent<Shoot>().canShoot = true;
			}
			else if(!networkView.isMine)
			{
				playerCamera.gameObject.SetActive(false);
				GetComponent<Shoot>().canShoot = false;
			}
		}
	}
	float GetRoundedAmount(float number)
	{
		number *= 100;
		Mathf.Round(number);
		number /= 100;
		return number;
	}
	void FixedUpdate()
	{
		if(Network.isClient || Network.isServer)
		{
			if(!networkView.isMine)
				SyncedMovement();
		}
	}
	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) 
	{
		
		Vector3 syncPosition = Vector3.zero;
		Vector3 syncVelocity = Vector3.zero;
		Quaternion syncRotation = m_rotation;
		
		
		if (stream.isWriting) 
		{
			syncPosition = m_transform.position;
			stream.Serialize(ref syncPosition);
			
			syncPosition = m_rigidbody.velocity;
			stream.Serialize(ref syncVelocity);
			
			syncRotation = rigidbody.rotation;
			stream.Serialize(ref syncRotation);
		} 
		else 
		{
			stream.Serialize(ref syncPosition);
			stream.Serialize(ref syncVelocity);
			stream.Serialize(ref syncRotation);
			m_rotation = syncRotation;
			m_transform.position = syncPosition;
			
			//TODO: Sync movement properly
			
//			m_syncTime = 0f;
//			m_syncDelay = Time.time - m_lastSynchronizationTime;
//			m_lastSynchronizationTime = Time.time;		
//			m_syncEndPosition = syncPosition + syncVelocity * m_syncDelay;
//			m_syncStartPosition = m_transform.position;
//			
//			syncPosition = m_transform.position;
		}
	}
	
	private void SyncedMovement()
	{
		m_syncTime += Time.deltaTime;
		//m_transform.position = Vector3.Lerp(m_syncStartPosition, m_syncEndPosition, m_syncTime / m_syncDelay);
		m_transform.rotation = Quaternion.Lerp(m_transform.rotation, m_rotation, m_syncTime / m_syncDelay);
	}
	void OnDisconnectedFromServer(NetworkDisconnection info)
	{
		Network.RemoveRPCs(networkView.viewID);
		Network.Destroy (networkView.viewID);
		Destroy(playerCamera);
	}
	[RPC]
	void SetPlayerName(string m_name)
	{
		playerName = m_name;
	}
	[RPC]
	void SetPlayerId(int m_id)
	{
		playerID = m_id;
	}
	[RPC] public void DisconnectPlayer()
	{
		Network.Disconnect(2);
		m_manager.showConnectionMenu = true;
		m_manager.menuCamera.gameObject.SetActive(true);
	}
	void OnGUI()
	{
		if(Network.isServer)
		{
			if(GUI.Button(new Rect(100,100,125,35),"Close Server"))
			{
				m_manager.DisconnectServer();
			}
		}
		else
		{
			if(GUI.Button(new Rect(100,100,125,35),"Disconnect"))
			{
				networkView.RPC("DisconnectPlayer",RPCMode.All);
				DisconnectPlayer();
				m_manager.showConnectionMenu = true;
				m_manager.menuCamera.gameObject.SetActive(true);
			}
		}
	}
}