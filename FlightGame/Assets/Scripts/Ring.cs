﻿using UnityEngine;
using System.Collections;

public class Ring : MonoBehaviour {

	public int order;
	public bool isActive = false;
	public bool done = false;
	public float speedIncrease;

	private RingManager m_manager;

	void Awake()
	{
		m_manager = FindObjectOfType<RingManager> ();
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.GetComponent<PlaneMovement>() && isActive)
		{
			col.GetComponent<PlaneMovement>().IncreaseSpeed(speedIncrease);
			m_manager.SetNextActive(order);
		}
	}
}