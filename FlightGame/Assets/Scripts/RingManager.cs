using UnityEngine;
using System.Collections;

public class RingManager : MonoBehaviour {

	public Ring[] rings;
	public Color32 activeColor, unactiveColor;
	public int progress;

	public bool isGameActive;
	public float time;

	private GameManager m_manager;

	void Awake()
	{
		m_manager = FindObjectOfType<GameManager> ();
		rings = FindObjectsOfType<Ring> ();
		SetFirstActive ();
	}
	void Update()
	{
		if(isGameActive)
		{
			Timer ();
			m_manager.UpdateRingGameStats();
		}
	}

	private void Timer()
	{
		time += Time.deltaTime;
	}

	private void SetFirstActive()
	{
		foreach(Ring ring in rings)
		{
			if(ring.order == 0)
			{
				ring.renderer.material.color = activeColor;
				ring.isActive = true;
			}
			else
			{
				ring.isActive = false;
				ring.renderer.material.color = unactiveColor;
			}
		}
	}

	public void SetNextActive(int currentIndex)
	{
		progress++;
		foreach(Ring ring in rings)
		{
			if(ring.order == currentIndex+1)
			{
				ring.renderer.material.color = activeColor;
				ring.isActive = true;
			}
			else
			{
				ring.isActive = false;
				ring.renderer.material.color = unactiveColor;
			}
		}
		if(progress >= rings.Length)
		{
			Victory();
		}
	}

	private void Victory()
	{
		Debug.Log ("Victory");
		isGameActive = false;

		m_manager.ShowEndScreen ();

	}
}