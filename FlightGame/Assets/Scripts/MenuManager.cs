﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

	public MenuObject[] menuObjects;

	public Slider qualitySlider;

	[System.Serializable]
	public class MenuObject
	{
		public Button activeButton;
		public GameObject content;
	}

	void Awake()
	{
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		foreach(MenuObject obj in menuObjects)
		{
			obj.content.SetActive(false);
		}
		
		qualitySlider.value = PlayerPrefs.GetInt ("quality");
	}

	public void ShowContent(int id)
	{
		for( int i = 0;i<menuObjects.Length;i++ )
		{
			if( i != id )
			{
				menuObjects[i].content.SetActive(false);
			}
			else
			{
				menuObjects[i].content.SetActive(true);
			}
		}
	}

	public void QuitGame()
	{
		Application.Quit();
	}

	public void ChangeQuality()
	{
		switch((int)qualitySlider.value)
		{
		case 1:
			QualitySettings.SetQualityLevel((int)QualityLevel.Fastest);
			PlayerPrefs.SetInt("quality",1);
			break;
		case 2:
			QualitySettings.SetQualityLevel((int)QualityLevel.Fast);
			PlayerPrefs.SetInt("quality",2);
			break;
		case 3:
			QualitySettings.SetQualityLevel((int)QualityLevel.Simple);
			PlayerPrefs.SetInt("quality",3);
			break;
		case 4:
			QualitySettings.SetQualityLevel((int)QualityLevel.Good);
			PlayerPrefs.SetInt("quality",4);
			break;
		case 5:
			QualitySettings.SetQualityLevel((int)QualityLevel.Beautiful);
			PlayerPrefs.SetInt("quality",5);
			break;
		case 6:
			QualitySettings.SetQualityLevel((int)QualityLevel.Fantastic);
			PlayerPrefs.SetInt("quality",6);
			break;
		}
	}

	public static void GetCurrentQualityLevel()
	{
		switch(PlayerPrefs.GetInt("quality"))
		{
		case 1:
			QualitySettings.SetQualityLevel((int)QualityLevel.Fastest);
			break;
		case 2:
			QualitySettings.SetQualityLevel((int)QualityLevel.Fast);
			break;
		case 3:
			QualitySettings.SetQualityLevel((int)QualityLevel.Simple);
			break;
		case 4:
			QualitySettings.SetQualityLevel((int)QualityLevel.Good);
			break;
		case 5:
			QualitySettings.SetQualityLevel((int)QualityLevel.Beautiful);
			break;
		case 6:
			QualitySettings.SetQualityLevel((int)QualityLevel.Fantastic);
			break;
		}
	}
	
	public void PlayMultiplayer()
	{
		Application.LoadLevel("test");
	}

	public void PlayRingGame()
	{
		Application.LoadLevel("Ring");
	}
	
	public void PlayTargetShootingGame()
	{
		Application.LoadLevel("Shooting");
	}

	public void PlayFreeGame()
	{
		Application.LoadLevel ("Free");
	}

	public void CalibrateControls()
	{
		Debug.Log (PlaneMovement.s_startPos);
		PlaneMovement.s_startPos = new Vector3 (-Input.acceleration.z, 0, -Input.acceleration.x);
		PlayerPrefs.SetFloat ("s_x", -Input.acceleration.z);
		PlayerPrefs.SetFloat ("s_z", -Input.acceleration.x);
	}
}