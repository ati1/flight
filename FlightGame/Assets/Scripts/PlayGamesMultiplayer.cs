﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Multiplayer;
using UnityEngine.UI;

public class PlayGamesMultiplayer : MonoBehaviour, RealTimeMultiplayerListener {

	public bool roomLoaded = false;
	public bool peerConnected = false;
	public float roomProgress = 0.0f;
	public bool messageReceived = false;
	private bool waitingForRoom = false;
	//private string mStatusText = "Ready.";
	
	private Invitation m_IncomingInvitation = null;
	
	[SerializeField]
	private Text m_logText;
	
	[SerializeField]
	private Text m_welcomeText;
	
	[SerializeField]
	private GameObject m_notConnectedMenu;
	
	[SerializeField]
	private GameObject m_connectedMenu;
	
	[SerializeField]
	private GameObject m_cancelButton;
	
	[SerializeField]
	private Button m_quickGame;
	
	[SerializeField]
	private GameObject m_leaveButton;

	private bool m_allowCancel;

	public PlayerController playerPrefab;
	public Camera playerCameraPrefab;
	
	private bool m_isMultiplayerReady;
	private string m_myId;
	private Vector3 m_startPos = new Vector3(1650,430,870);
	
	[SerializeField]
	private Camera menuCamera;
	
	[SerializeField]
	private GameObject connectionUI;

	[SerializeField]
	private Transform[] m_spawnPoints;
	
	public PlayerController myPlane;
	
	[SerializeField]
	private PlayerController[] m_planes;

	[SerializeField]
	private GameObject m_healthBar;
	
	public Dictionary<string,PlayerController> playerCharacters = new Dictionary<string, PlayerController>();

	private Dictionary<string,int> m_planeIndexes = new Dictionary<string, int>();
	
	private List<byte> m_planeSyncMessage = new List<byte> ();

	public static float s_timePerUpdate = 0.16f;
	private float m_nextBroadcastTime = 0;

	private List<byte> m_planeRemoveMessage = new List<byte> ();
	
	#region RealTimeMultiplayerListener implementation

	public void OnRoomSetupProgress (float percent)
	{
		roomProgress = percent;
	}

	public void OnRoomConnected (bool success)
	{
		if (success) 
		{
			m_allowCancel = false;
			waitingForRoom = false;
			peerConnected = true;
			roomLoaded = success;
			InitGame();
		} 
		else 
		{
			m_allowCancel = false;
			waitingForRoom = false;
			m_logText.text = "Error while trying to connect to room";
		}
	}

	public void OnLeftRoom ()
	{
		peerConnected = false;
		m_logText.text = "Left room";
		roomLoaded = false;
		m_connectedMenu.SetActive(true);
		m_notConnectedMenu.SetActive(false);
		m_leaveButton.SetActive(false);
	}

	public void OnPeersConnected (string[] participantIds)
	{

	}

	public void OnPeersDisconnected (string[] participantIds)
	{
//		foreach (string participantID in participantIds) 
//		{
//			if(!playerCharacters.ContainsKey(participantID))
//			{
//
//			}
//		}
	}

	public void OnRealTimeMessageReceived (bool isReliable, string senderId, byte[] data)
	{
		byte messageVersion = (byte)data[0];
		char messageType = (char)data[1];
		PlayerController controllerToUpdate;
		switch(messageType)
		{
		case 'U':
			float posX = System.BitConverter.ToSingle(data, 2);
			float posY = System.BitConverter.ToSingle(data, 6);
			float posZ = System.BitConverter.ToSingle(data, 10);
			float rotX = System.BitConverter.ToSingle(data, 14);
			float rotY = System.BitConverter.ToSingle(data, 18);
			float rotZ = System.BitConverter.ToSingle(data, 22);
			float rotW = System.BitConverter.ToSingle(data, 26);
			
			//do update for specific player
			playerCharacters.TryGetValue(senderId, out controllerToUpdate);
			
			if(controllerToUpdate != null)
			{
//				controllerToUpdate.transform.position = new Vector3(posX,posY,posZ);
//				controllerToUpdate.transform.rotation = new Quaternion(rotX,rotY,rotZ,rotW);
				controllerToUpdate.ReceivePlaneUpdate(posX,posY,posZ,rotX,rotY,rotZ,rotW);
			}
			break;
		case 'S':
			int planeIndex = System.BitConverter.ToInt32(data,2);
			m_planeIndexes.Add(senderId,planeIndex);
			break;
		case 'F':
			float shootPosX = System.BitConverter.ToSingle(data, 2);
			float shootPosY = System.BitConverter.ToSingle(data, 6);
			float shootPosZ = System.BitConverter.ToSingle(data, 10);
			float shootRotX = System.BitConverter.ToSingle(data, 14);
			float shootRotY = System.BitConverter.ToSingle(data, 18);
			float shootRotZ = System.BitConverter.ToSingle(data, 22);
			float shootRotW = System.BitConverter.ToSingle(data, 26);

			playerCharacters.TryGetValue(senderId, out controllerToUpdate);

			if(controllerToUpdate != null)
			{
				controllerToUpdate.GetComponent<Shoot>().ShootAmmo(new Vector3(shootPosX,shootPosY,shootPosZ),new Quaternion(shootRotX,shootRotY,shootRotZ,shootRotW));
			}
			break;
		case 'H':
			float damage = System.BitConverter.ToSingle(data, 2);
			string id = System.BitConverter.ToString(data, 6);

			playerCharacters.TryGetValue(id, out controllerToUpdate);
			
			if(controllerToUpdate != null)
			{
				controllerToUpdate.GetComponent<Health>().AdjustHealth(-damage);
			}

			break;
		case 'D':
			playerCharacters.TryGetValue(senderId, out controllerToUpdate);
			
			if(controllerToUpdate != null)
			{
				controllerToUpdate.GetComponent<Health>().Die();
			}
			break;
		case 'R':
			m_logText.text = "Player " + senderId + " left game";
			RemovePlane(senderId);
			break;
		}

	}

	#endregion
	
	void Awake()
	{
		m_leaveButton.SetActive(false);
		PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
		.WithInvitationDelegate(OnInvitationReceived) 
		.Build();
		
		PlayGamesPlatform.InitializeInstance(config);
		
		// recommended for debugging:
		PlayGamesPlatform.DebugLogEnabled = true;
		
		// Activate the Google Play Games platform
		PlayGamesPlatform.Activate();
		m_connectedMenu.SetActive(false);
		m_notConnectedMenu.SetActive(true);
	}
	
	// called when an invitation is received:
	public void OnInvitationReceived(Invitation invitation, bool shouldAutoAccept) 
	{
		if (shouldAutoAccept) 
		{
			// Invitation should be accepted immediately. This happens if the user already
			// indicated (through the notification UI) that they wish to accept the invitation,
			// so we should not prompt again.
			//ShowWaitScreen();
			PlayGamesPlatform.Instance.RealTime.AcceptInvitation(invitation.InvitationId, this);
		} 
		else
		{
			// The user has not yet indicated that they want to accept this invitation.
			// We should *not* automatically accept it. Rather we store it and 
			// display an in-game popup:
			m_IncomingInvitation = invitation;
		}
	}
	
	
	public void SignIn()
	{
		m_logText.text = "Authenticating...";
		Social.localUser.Authenticate((bool success) => {
			m_logText.text = success ? "Successfully authenticated" : "Authentication failed.";
			
			if(success)
			{
				m_connectedMenu.SetActive(true);
				m_notConnectedMenu.SetActive(false);
				m_welcomeText.text = "Welcome " + Social.localUser.userName + "!";
			}
			else
			{
				m_connectedMenu.SetActive(false);
				m_notConnectedMenu.SetActive(true);
			}
		});
	}
	
	public void SignOut()
	{
		((PlayGamesPlatform)Social.Active).SignOut();
		m_connectedMenu.SetActive(false);
		m_notConnectedMenu.SetActive(true);
		m_logText.text = "Signing out.";
	}
	
	public void QuickGame()
	{
		waitingForRoom = true;
		const int MinOpponents = 1, MaxOpponents = 5;
		const int GameVariant = 0;
		
		PlayGamesPlatform.Instance.RealTime.CreateQuickGame(MinOpponents, MaxOpponents,
		                                                    GameVariant, this);
	}
	
	public void CreateWithInvitationScreen()
	{
		const int MinOpponents = 1, MaxOpponents = 3;
		const int GameVariant = 0;
		PlayGamesPlatform.Instance.RealTime.CreateWithInvitationScreen(MinOpponents, MaxOpponents,
		                                                               GameVariant, this);
	}
	
	public void AcceptInvitationFromInBox()
	{
		PlayGamesPlatform.Instance.RealTime.AcceptFromInbox(this);
	}
	
	public void AcceptInvitation()
	{
		Invitation invitation = m_IncomingInvitation;  // (obtained via delegate)
		if(invitation != null)
			PlayGamesPlatform.Instance.RealTime.AcceptInvitation(invitation.InvitationId, this);
	}
	
	public void InitGame()
	{
		m_logText.text = "Connected to a game!\nThere is " + GetParticipants().Count + " players in total";
		m_connectedMenu.SetActive(false);
		m_notConnectedMenu.SetActive(false);
		m_leaveButton.SetActive(true);
		connectionUI.SetActive(false);
		StartCoroutine ("WaitForSync");
		//SetupMultiplayer();
	}

	IEnumerator WaitForSync()
	{
		SyncPlane(PlayerPrefs.GetInt("plane"));
		while (m_planeIndexes.Count == 0)
			yield return null;
		SetupMultiplayer ();
	}
	
	public List<Participant> GetParticipants()
	{
		return PlayGamesPlatform.Instance.RealTime.GetConnectedParticipants();
	}
	
	public void SendMessageToEveryone(bool reliable, byte[] message)
	{
		PlayGamesPlatform.Instance.RealTime.SendMessageToAll(reliable, message);
	}
	
	public void SendMessageToInvidual(bool reliable, byte[] message, string participantId)
	{
		PlayGamesPlatform.Instance.RealTime.SendMessage(reliable, participantId, message);
	}
	
	public void GameFinished()
	{
		for(int i = 0;i<GetParticipants().Count;i++)
		{
			PlayGamesPlatform.Instance.RealTime.LeaveRoom();
		}
	}
	
	public void CancelRoomLoading()
	{
		m_logText.text = "Loading room cancelled.";
		waitingForRoom = false;
		PlayGamesPlatform.Instance.RealTime.LeaveRoom();
		m_quickGame.enabled = true;
	}
	
	public void LeaveRoom()
	{
		//TODO: fix this it doesn't work.
		RemoveMyPlane();

		PlayerController myPlane = null;
		playerCharacters.TryGetValue (GetMyId (), out myPlane);
		
		Destroy (myPlane.GetComponent<PlaneMovement> ().playerCamera.gameObject);
		Destroy (myPlane.gameObject);

		PlayGamesPlatform.Instance.RealTime.LeaveRoom();

		menuCamera.gameObject.SetActive (true);

		m_isMultiplayerReady = false;
		connectionUI.SetActive (true);
		m_healthBar.SetActive(false);
	}

	public void RemovePlane(string playerId)
	{
		PlayerController myPlane;
		if(playerCharacters.TryGetValue (playerId, out myPlane))
		{
			Destroy (myPlane);
			playerCharacters.Remove (playerId);
		}
	}

	public void RemoveMyPlane()
	{
		byte m_protocolVersion = 1;
		m_planeRemoveMessage.Clear ();
		m_planeRemoveMessage.Add (m_protocolVersion);
		m_planeRemoveMessage.Add ((byte)'R');
		
		byte[] messageToSend = m_planeRemoveMessage.ToArray();
		SendMessageToEveryone(false, messageToSend);
	}
	
	void Update()
	{
		if(waitingForRoom)
		{
			if(this.roomProgress < 20)
			{
				m_logText.text = "Loading room... " + this.roomProgress.ToString() + "%";
			}
			else
			{
				m_logText.text = "Room ready. Waiting for players..";
			}

			m_allowCancel = true;
			m_quickGame.enabled = false;
		}
		else if( !peerConnected && !waitingForRoom)
		{
			m_quickGame.enabled = true;
		}
		
		if(m_allowCancel)
		{
			if(!m_cancelButton.activeInHierarchy)
			{
				m_cancelButton.SetActive(true);
			}
		}
		else
		{
			if(m_cancelButton.activeInHierarchy)
			{
				m_cancelButton.SetActive(false);
			}
		}
		
		if(m_isMultiplayerReady)
		{
			MultiplayerUpdate();
		}
	}
	
	public void LoadMenu()
	{
		if(peerConnected)
			LeaveRoom();
		SignOut();
		Application.LoadLevel(0);
	}
		
//		if (m_IncomingInvitation != null) 
//		{
//			// show the popup
//			string who = (m_IncomingInvitation.Inviter != null && m_IncomingInvitation.Inviter.DisplayName != null) ? m_IncomingInvitation.Inviter.DisplayName : "Someone";
//			GUILayout.Label(who + " is challenging you to a race!");
//			
//			if (GUILayout.Button("Accept!")) 
//			{
//				// user wants to accept the invitation!
//				//ShowWaitScreen();
//				PlayGamesPlatform.Instance.RealTime.AcceptInvitation(m_IncomingInvitation.InvitationId, this);
//			}
//			
//			if (GUILayout.Button("Decline")) 
//			{
//				// user wants to decline the invitation
//				PlayGamesPlatform.Instance.RealTime.DeclineInvitation(m_IncomingInvitation.InvitationId);
//			}
//		}
//	}
	public Vector3 GetRandomSpawnPoint()
	{
		int rndSpot = Random.Range (0, m_spawnPoints.Length);
		return m_spawnPoints [rndSpot].position;
	}

	public string GetMyId()
	{
		return PlayGamesPlatform.Instance.RealTime.GetSelf().ParticipantId;
	}

	public void SetupMultiplayer()
	{
		m_myId = GetMyId ();
		
		List<Participant> allPlayers = GetParticipants();
		
		// init player planes
		for (int i =0; i < allPlayers.Count; i++)
		{
			string nextParticipantId = allPlayers[i].ParticipantId;
			
			if (nextParticipantId == m_myId)
			{
				//Instantiate player plane
				GameObject go = GameObject.Instantiate(m_planes[PlayerPrefs.GetInt("plane")].gameObject,GetRandomSpawnPoint(),Quaternion.identity) as GameObject;
				GameObject cam = GameObject.Instantiate(playerCameraPrefab.gameObject) as GameObject;
				myPlane = go.GetComponent<PlayerController>();
				menuCamera.gameObject.SetActive(false);
				
				myPlane.GetComponent<PlaneMovement> ().playerCamera = cam.camera;
				//m_logText.text = "Set up camera " + myPlane.GetComponent<PlaneMovement> ().playerCamera.ToString();
				myPlane.GetComponent<PlayerNetworkValues>().enabled = false;
				myPlane.GetComponent<PlayerController>().myParticipantId = PlayGamesPlatform.Instance.RealTime.GetSelf().ParticipantId;
				HealthBar hpBar = myPlane.GetComponent<HealthBar>();
				hpBar.showOldGUI = false;
				m_healthBar.SetActive(true);
				hpBar.InitHealthBar();
				hpBar.useHealthBar = true;

				playerCharacters.Add(m_myId,myPlane);
			} 
			else 
			{
				//Enemy Plane
				int index = 0;

				m_planeIndexes.TryGetValue(allPlayers[i].ParticipantId, out index);

				GameObject go = GameObject.Instantiate(m_planes[index].gameObject,m_startPos,Quaternion.identity) as GameObject;
				go.GetComponent<PlaneMovement>().enabled = false;
				go.GetComponent<PlayerNetworkValues>().enabled = false;
				go.GetComponent<PlayerController>().crosshair.SetActive(false);

				HealthBar hpBar = go.GetComponent<HealthBar>();
				hpBar.InitHealthBar();
				hpBar.useHealthBar = true;
				
				playerCharacters.Add(nextParticipantId,go.GetComponent<PlayerController>());
			}
		}
		
		m_isMultiplayerReady = true;
	}

	public void PrintMessage(string message)
	{
		m_logText.text = message;
	}
	
	public void MultiplayerUpdate()
	{
		if (Time.time > m_nextBroadcastTime)
		{
			myPlane.SendMyUpdate(myPlane.transform.position.x, myPlane.transform.position.y, myPlane.transform.position.z,
			myPlane.transform.rotation.x, myPlane.transform.rotation.y, myPlane.transform.rotation.z, myPlane.transform.rotation.w);

			m_nextBroadcastTime = Time.time + 0.16f;
		}
	}

	public void SyncPlane(int index)
	{
		byte m_protocolVersion = 1;
		m_planeSyncMessage.Clear ();
		m_planeSyncMessage.Add (m_protocolVersion);
		m_planeSyncMessage.Add ((byte)'S');
		
		m_planeSyncMessage.AddRange (System.BitConverter.GetBytes (index));
		
		byte[] messageToSend = m_planeSyncMessage.ToArray();
		SendMessageToEveryone(false, messageToSend);
	}
}