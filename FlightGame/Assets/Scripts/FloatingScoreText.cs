﻿using UnityEngine;
using System.Collections;

public class FloatingScoreText : MonoBehaviour {
	
	public float scrollSpeed = 0;
	[HideInInspector]
	public TextMesh textMesh;
	private Transform m_transform;
	private GameObject playerCamera;
	
	void Awake() 
	{
		m_transform = GetComponent<Transform> ();
		textMesh = GetComponent<TextMesh> ();
		playerCamera = GameObject.FindGameObjectWithTag("MainCamera");
	}
	
	void Update () 
	{
		m_transform.Translate(Vector2.up * scrollSpeed * Time.deltaTime);
		m_transform.rotation = Quaternion.LookRotation(playerCamera.transform.position - new Vector3(transform.position.x,transform.position.y,transform.position.z));
	}
}