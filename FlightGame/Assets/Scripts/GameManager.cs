﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public enum GameType
	{
		ring,
		target,
		free
	};
	public GameObject playerPrefab;
	public Camera playerCamera;
	public Transform startPos;

	public GameObject[] menuObjects;
	public GameObject gameScreen;
	public Text collected;
	public Text time;
	public GameObject endScreen;
	public Text endTime;
	
	public Text shootingScore;
	public Text shootingTargets;

	public GameType gameType;
	public GameObject manager;

	private RingManager m_ringManager;
	private TargetShootingManager m_targetManager;
	
	[SerializeField]
	private GameObject[] m_planes;
	

	void Awake()
	{
		MenuManager.GetCurrentQualityLevel ();
		InitPlane();
		InitGameType ();
	}
	
	void InitPlane()
	{
		playerPrefab = m_planes[PlayerPrefs.GetInt("plane")];
	}
	

	private void InitGameType()
	{
		switch(gameType)
		{
		case GameType.ring:
			InitRingGame();
			break;
		case GameType.target:
			InitShootingGame();
			break;
		case GameType.free:
			InitFreeGame();
			break;
		}
	}

	private void InitFreeGame()
	{
		GameObject go = GameObject.Instantiate(playerPrefab,startPos.position,Quaternion.identity) as GameObject;
		go.GetComponent<PlayerNetworkValues> ().enabled = false;
		GameObject cam = GameObject.Instantiate(playerCamera.gameObject) as GameObject;
		go.GetComponent<PlaneMovement> ().playerCamera = cam.camera;
		go.GetComponent<PlayerController> ().enabled = false;
	}

	private void InitRingGame()
	{
		GameObject temp = GameObject.Instantiate (manager, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
		m_ringManager = temp.GetComponent<RingManager> ();
		gameScreen.SetActive (false);
		endScreen.SetActive (false);

		for( int i = 0;i<menuObjects.Length;i++ )
		{
			menuObjects[i].SetActive(true);
		}

		m_ringManager.isGameActive = false;
	}

	public void StartRingGame()
	{
		for( int i = 0;i<menuObjects.Length;i++ )
		{
			menuObjects[i].SetActive(false);
		}

		GameObject go = GameObject.Instantiate(playerPrefab,startPos.position,Quaternion.identity) as GameObject;
		go.GetComponent<PlayerNetworkValues> ().enabled = false;
		GameObject cam = GameObject.Instantiate(playerCamera.gameObject) as GameObject;
		go.GetComponent<PlaneMovement> ().playerCamera = cam.camera;
		gameScreen.SetActive (true);
		m_ringManager.isGameActive = true;
		go.GetComponent<PlayerController> ().enabled = false;

	}

	public void ShowEndScreen()
	{
		switch(gameType)
		{
			case GameType.ring:
				gameScreen.SetActive (false);
				endScreen.SetActive (true);
				endTime.text = "Your time was: " + m_ringManager.time.ToString ("F2");
			break;
			case GameType.target:
				gameScreen.SetActive (false);
				endScreen.SetActive (true);
			endTime.text = "Your score was: " + m_targetManager.score + "\n You had " + m_targetManager.amountLeft() + " Targets Left" + 
				"\n You had " + m_targetManager.time.ToString("F2") + " Time Left";
			break;
		}
	}

	public void UpdateRingGameStats()
	{
		time.text = "Time: " +  m_ringManager.time.ToString("F2");
		collected.text = "Rings " + m_ringManager.progress + "/" + m_ringManager.rings.Length;
	}
	
	public void InitShootingGame()
	{
		GameObject temp = GameObject.Instantiate (manager, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
		m_targetManager = temp.GetComponent<TargetShootingManager> ();
		gameScreen.SetActive (false);
		endScreen.SetActive (false);
		
		for( int i = 0;i<menuObjects.Length;i++ )
		{
			menuObjects[i].SetActive(true);
		}
		
		m_targetManager.isGameActive = false;
		
	}
	
	public void StartShootingGame()
	{
		for( int i = 0;i<menuObjects.Length;i++ )
		{
			menuObjects[i].SetActive(false);
		}
		
		GameObject go = GameObject.Instantiate(playerPrefab,startPos.position,Quaternion.identity) as GameObject;
		go.GetComponent<PlayerNetworkValues> ().enabled = false;
		GameObject cam = GameObject.Instantiate(playerCamera.gameObject) as GameObject;
		go.GetComponent<PlaneMovement> ().playerCamera = cam.camera;
		gameScreen.SetActive (true);
		m_targetManager.isGameActive = true;
		go.GetComponent<PlayerController> ().enabled = false;
	}
	
	public void UpdateShootGameStats()
	{
		time.text = "Time Left: " + m_targetManager.time.ToString("F2");
		shootingScore.text = "Score: " + m_targetManager.score;
		shootingTargets.text = "Targets Left: " + m_targetManager.amountLeft();
		
	}

	public void Retry()
	{
		Application.LoadLevel (Application.loadedLevel);
	}

	public void ReturnMenu()
	{
		Application.LoadLevel ("Menu");
	}
}