﻿using UnityEngine;
using System.Collections;

public class TargetShootingManager : MonoBehaviour {

	public Target[] targets;
	
	public int amountLeft()
	{
		int amount = 0;
		foreach( Target target in targets )
		{
			if(!target.isShot)
			{
				amount++;
			}
		}
		return amount;
	}
	
	public int amountInStart;
	public int score;
	
	public float stageTime;

	public float time;

	public bool isGameActive = false;

	private GameManager m_manager;
	
	void Awake()
	{
		targets = FindObjectsOfType<Target>();
		amountInStart = targets.Length;
		m_manager = FindObjectOfType<GameManager>();
		time = stageTime;
	}
	
	void Update()
	{
		if(isGameActive)
		{
			DoTimer();
			m_manager.UpdateShootGameStats();
		}
	}
	
	public void AddScore(int amount)
	{
		score += amount;
		if(amountLeft() == 0)
		{
			EndGame();
			m_manager.ShowEndScreen();
		}
	}
	
	void DoTimer()
	{
		time -= Time.deltaTime;
		if(time <= 0)
		{
			time = 0;
			if(isGameActive)
			{
				EndGame();
			}
		}
	}
	
	public void EndGame()
	{
		isGameActive = false;
		foreach(Target target in targets)
		{
			target.gameObject.SetActive(false);
		}
		m_manager.ShowEndScreen();
	}
}