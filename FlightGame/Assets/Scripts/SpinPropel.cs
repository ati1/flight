﻿using UnityEngine;
using System.Collections;

public class SpinPropel : MonoBehaviour {
	
	public float spinSpeed;
	private Transform m_transform;
	[SerializeField]
	private Vector3 m_angles;
	
	void Awake()
	{
		m_transform = transform;
	}
	
	void FixedUpdate()
	{
		m_transform.Rotate(m_angles * spinSpeed * Time.deltaTime);
	}
}