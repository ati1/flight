﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlaneSelection : MonoBehaviour {

	public Camera sceneCamera;
	
	private int m_currentIndex = 0;
	
	[SerializeField]
	private Vector3 m_planeSpot;
	
	[SerializeField]
	private Transform m_lookAtPos;
	
	[SerializeField]
	private Text planeInfo;
	
	public GameObject instantiatedPlaneModel;
	public GameObject[] planes;
	public GameObject currentPlane;
	[Multiline]
	public string[] planeInfos;

	void Awake()
	{
		m_currentIndex = PlayerPrefs.GetInt("plane");
		SetPlane(m_currentIndex);
	}
	
	public void SetPlane(int index)
	{
		currentPlane = planes[index];
		if(instantiatedPlaneModel != null)
			Destroy(instantiatedPlaneModel);
		instantiatedPlaneModel = (GameObject)GameObject.Instantiate(currentPlane,m_planeSpot,Quaternion.identity);
		planeInfo.text = planeInfos[index];
		PlayerPrefs.SetInt("plane", index);
	}
	
	public void ChangePlane(bool direction)
	{
		if(direction && m_currentIndex < planes.Length - 1)
		{
			m_currentIndex += 1;
			SetPlane(m_currentIndex);
		}
		else if(!direction && m_currentIndex > 0)
		{
			m_currentIndex -= 1;
			SetPlane(m_currentIndex);
		}
	}
	
	void FixedUpdate()
	{
		sceneCamera.transform.LookAt(instantiatedPlaneModel.transform.position);
		sceneCamera.transform.RotateAround(instantiatedPlaneModel.transform.position, new Vector3(0,1,0), .45f);
	}
}