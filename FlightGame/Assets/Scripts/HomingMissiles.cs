﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HomingMissiles : MonoBehaviour {

	public float missileVelocity = 100;
	public float turn = 20;
	public Rigidbody homingMissile;
	public float fuseDelay;
	public GameObject missileMod;
	public ParticleSystem SmokePrefab;
	public AudioClip missileClip;
	private Transform target;
	
	void Awake () 
	{
		homingMissile = rigidbody;
	}
	
	void FixedUpdate ()	
	{
		if(target == null || homingMissile == null)
			return;
		
		homingMissile.velocity = transform.forward * missileVelocity;
		
		Quaternion targetRotation = Quaternion.LookRotation(target.position - transform.position);
		
		homingMissile.MoveRotation(Quaternion.RotateTowards(transform.rotation, targetRotation, turn));
		
	}
	
	public void Fire ()
	{
		//AudioSource.PlayClipAtPoint(missileClip, transform.position);
		
		float distance = Mathf.Infinity;
		List<GameObject> targets = new List<GameObject>();

		if(FindObjectOfType<TargetShootingManager>())
		{
			Target[] tars = FindObjectsOfType<Target>();
			for( int i = 0; i < tars.Length; i++ )
			{
				targets.Add(tars[i].gameObject);
			}
		}

		Health[] healths = FindObjectsOfType<Health>();
		for( int i = 0; i < healths.Length; i++ )
		{
			if(!healths[i].GetComponent<NetworkView>().isMine)
				targets.Add(healths[i].gameObject);
		}

		for ( int i = 0 ; i< targets.Count;i++ )
		{
			float diff = (targets[i].transform.position - transform.position).sqrMagnitude;
			
			if(diff < distance)
			{
				distance = diff;
				target = targets[i].transform;
			}
		}
		
	}
	
	void OnCollisionEnter (Collision col)
	{
		
		if(col.gameObject.name == "Cube")
		{
			SmokePrefab.emissionRate = 0.0f;
			Destroy(missileMod.gameObject);
			Destroy(gameObject);
		}
		
	}
}