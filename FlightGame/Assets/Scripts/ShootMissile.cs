﻿using UnityEngine;
using System.Collections;

public class ShootMissile : MonoBehaviour {

	public int maxAmmos;
	public int currentAmmo;
	public GameObject missile;
	private Transform m_transform;

	public bool rdyToShoot;
	private float m_shootTimer;
	public float shootDelay;

	public bool canShoot;

	void Awake()
	{
		m_transform = transform;
		m_shootTimer = shootDelay;
	}

	void FixedUpdate()
	{
		if(canShoot)
		{
			Shoot();
		}
	}

	void Shoot()
	{
		#if UNITY_ANDROID
		if(Input.touchCount > 0 && Input.GetTouch(0).tapCount > 2 && Input.GetTouch(0).phase != TouchPhase.Ended && Input.GetTouch(0).phase != TouchPhase.Canceled && currentAmmo > 0 && rdyToShoot)
		#elif UNITY_STANDALONE_WIN
		if(Input.GetKeyDown(KeyCode.Mouse1) && currentAmmo > 0 && rdyToShoot)
		#endif
		{
			rdyToShoot = false;
			m_shootTimer = shootDelay;
			FireMissile();
			currentAmmo--;
		}
		
		if(!rdyToShoot && m_shootTimer <= 0f)
			rdyToShoot = true;

		if(m_shootTimer >= 0)
			m_shootTimer--;
	}

	void FireMissile()
	{
		GameObject tempMissile = GameObject.Instantiate (missile, m_transform.position - new Vector3(0,5,0), Quaternion.identity) as GameObject;
		tempMissile.GetComponent<HomingMissiles> ().Fire ();
	}

	void OnGUI()
	{
		if(GetComponent<NetworkView>().isMine)
		{
			GUI.color = Color.black;
			GUI.Label(new Rect(20,285,150,35),"Missiles:" + currentAmmo + "/" + maxAmmos);
		}
	}
}