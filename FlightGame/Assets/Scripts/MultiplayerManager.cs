﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MultiplayerManager : MonoBehaviour {
	
	public Camera menuCamera;
	
	const string typeName = "flightGame";
	public string gameName = "";
	
	public string playerName = "";
	
	public GameObject playerPrefab;
	
	//public List<NetworkPlayer> players;
	public List<PlayerNetworkValues> players;
	
	[HideInInspector]
	public bool showConnectionMenu = true;
	
	private float searchTime = 0;
	public float maxSearchTime = 0;
	private bool isRefreshingHostList = false;
	private List<HostData> hostList = new List<HostData>();
	private bool hostGame;
	private bool joinGame;
	
	public enum RequestType
	{
		getplayerlist
	};
	public RequestType requestType;
	
	public Transform[] spawnSpots;
	
	void Awake () 
	{
		searchTime = maxSearchTime;
		//networkFunctions.Add("getplayerlist",GetNetworkPlayerList);
	}
	void Update()
	{
		if(isRefreshingHostList)
			SearchForHosts();
	}
	/// <summary>
	/// Creates host.
	/// </summary>
	private void StartServer()
	{
		Network.InitializeServer(5, 25000, !Network.HavePublicAddress());
		MasterServer.RegisterHost(typeName, gameName);
	}
	/// <summary>
	/// Searchs for hosts for set amount of time.
	/// </summary>
	private void SearchForHosts()
	{
		if (isRefreshingHostList) 
		{
			if (searchTime <= 0) 
			{
				searchTime = 0;
			}
			if (searchTime == 0) 
			{
				isRefreshingHostList = false;
				searchTime = maxSearchTime;
			}
			if (MasterServer.PollHostList().Length > 0)
			{
				isRefreshingHostList = false;
				hostList.Clear();
				hostList.AddRange (MasterServer.PollHostList());
			}
			searchTime -= Time.deltaTime;
		}
		else
		{
			HostData[] tempHostData = MasterServer.PollHostList();
			if(tempHostData.Length != hostList.Count)
			{
				hostList.Clear();
				if(tempHostData.Length > 0)
				{
					hostList.AddRange(tempHostData);
				}
			}
		}
	}
	/// <summary>
	/// Start to refresh hostlist and search for hosts with same unique typeName.
	/// </summary>
	private void RefreshHostList()
	{
		if (!isRefreshingHostList)
		{
			isRefreshingHostList = true;
			MasterServer.RequestHostList(typeName);
		}
	}
	/// <summary>
	/// Is called when Host is created.
	/// </summary>
	private void OnServerInitialized()
	{
		showConnectionMenu = false;
		SpawnPlayer();
	}
	/// <summary>
	/// Client joins the server using this
	/// </summary>
	/// <param name="hostData">Host data.</param>
	private void JoinServer(HostData hostData)
	{
		Network.Connect(hostData);
		gameName = hostData.gameName;
	}
	/// <summary>
	/// Is called when new player connects to host.
	/// </summary>
	private void OnConnectedToServer()
	{
		showConnectionMenu = false;
		SpawnPlayer();
		//networkFunctions ["SetPlayerId"] ();
	}
	
	//	public int GetPlayerId()
	//	{
	//		//connections = GetNetworkConnections();
	//	}
	
	public void HandleRequest(RequestType requestType)
	{
		if(!Network.isServer)
			return;
		
		switch(requestType)
		{
		case RequestType.getplayerlist:
			
			break;
		}
	}
	/// <summary>
	/// Returns array of connections
	/// </summary>
	/// <returns>The player list.</returns>
	public NetworkPlayer[] GetNetworkConnections()
	{
		return Network.connections;
	}
	
	void OnGUI()
	{
		GUILayout.BeginArea (new Rect (Screen.width / 2 - 200, Screen.height / 2 - 200, 1000, 1000));
		if(!Network.isClient && !Network.isServer)
		{
			GUILayout.BeginHorizontal ();
			if(GUILayout.Button ("Host Game", GUILayout.Width (100)))
			{
				hostGame = true;
				joinGame = false;
			}
			if(GUILayout.Button ("Join Game", GUILayout.Width (100)))
			{
				hostGame = false;
				joinGame = true;
			}
			GUILayout.EndHorizontal ();
			GUILayout.BeginVertical ();
			if(hostGame)
			{
				GUILayout.Label("Name of your character",GUILayout.Width(100));
				playerName = GUILayout.TextArea(playerName,GUILayout.Width(100));
				GUILayout.Label("Name of your game",GUILayout.Width(100));
				gameName = GUILayout.TextArea(gameName,GUILayout.Width(100));
				if(GUILayout.Button("Create Game",GUILayout.Width (100),GUILayout.Height(45)) && gameName != "" && gameName != "Game Name" && playerName != "")
				{
					StartServer();
				}
			}
			else if(joinGame)
			{
				GUILayout.Label("Name of your character",GUILayout.Width(100));
				playerName = GUILayout.TextArea(playerName,GUILayout.Width(100));
				if(GUILayout.Button("Search for games",GUILayout.Width(150)))
				{
					RefreshHostList();
				}
				if (hostList != null)
				{
					GUILayout.Space(10);
					for (int i = 0; i < hostList.Count; i++)
					{
						if (GUILayout.Button( hostList[i].gameName,GUILayout.Width(100)) && playerName != "")
							JoinServer(hostList[i]);
					}
				}
			}
			if(GUILayout.Button ("Back to menu", GUILayout.Width (100)))
			{
				Application.LoadLevel("Menu");
			}
			GUILayout.EndVertical ();
		}
		else
		{
			if(Network.isServer)
				GUILayout.Label("Connected to server: " +  gameName + " as host");
			else if(Network.isClient)
				GUILayout.Label("Connected to server: " + gameName + " as client");
		}
		
		GUILayout.EndArea ();
	}
	/// <summary>
	/// Spawn the player and initialize it.
	/// </summary>
	public void SpawnPlayer()
	{
		GameObject go = Network.Instantiate(playerPrefab,GetRandomSpawnPoint().position,Quaternion.identity,0) as GameObject;
		menuCamera.gameObject.SetActive (false);
		PlayerNetworkValues values = go.GetComponent<PlayerNetworkValues> ();
		values.networkView.RPC ("SetPlayerName", RPCMode.AllBuffered, playerName);
		//values.networkView.RPC ("SetPlayerId", RPCMode.AllBuffered, GetPlayerId());
		//		ServerHandler handler = go.GetComponent<ServerHandler> ();
		//		handler.InitServerMethodDictionary();
		//		handler.InitClientMethodDictionary();
		//		handler.player.playerName = playerName;
		//		handler.InitPlayer ();
	}
	public int GetPlayerId()
	{
		return FindObjectsOfType<PlayerNetworkValues> ().Length;
	}
	/// <summary>
	/// Return random spawn point.
	/// </summary>
	/// <returns>The random spawn point.</returns>
	public Transform GetRandomSpawnPoint()
	{
		Transform spawnSpot = spawnSpots[Random.Range(0,spawnSpots.Length)];
		return spawnSpot;
	}
	
	public void DisconnectServer()
	{
		if (!Network.isServer) 
		{
			return;
		}
		PlayerNetworkValues[] tempPlayers = FindObjectsOfType<PlayerNetworkValues>();
		foreach(PlayerNetworkValues temp in tempPlayers)
		{
			temp.DisconnectPlayer();
		}
		Network.Disconnect();
		MasterServer.UnregisterHost();
		showConnectionMenu = true;
		menuCamera.gameObject.SetActive(true);
	}
	
	void OnPlayerDisconnected(NetworkPlayer player)
	{
		if (Network.isServer) 
		{
			Network.RemoveRPCs (player);
			Network.DestroyPlayerObjects (player);
		}
	}
}