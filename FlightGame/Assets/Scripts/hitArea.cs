﻿using UnityEngine;
using System.Collections;

public class hitArea : MonoBehaviour {

	public int score;
	private Target m_target;
	
	void Awake()
	{
		m_target = transform.parent.GetComponent<Target>();
	}
	
	void OnTriggerEnter(Collider col)
	{
		if(col.GetComponent<ColliderScript>())
		{
			m_target.DoHit(score);
		}
	}
}