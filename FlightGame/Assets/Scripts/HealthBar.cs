﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

	public bool useHealthBar;
	public bool showOldGUI;

	private GameObject healthBar;
	public RectTransform rectTrans;
	
	public Texture2D healthBarTexture;
	public Texture2D dmgTakenTexture;
	private Health m_health;
	private Transform m_transform;
	public float hpBarOffsetY = 0;
	public float hpBarOffsetX = 0;

	public float healthbarLenght;
	private float m_maxHealthBarLength = 0;

	public float GUIHealthBarLenght;
	private float m_maxGUIHealthBarLenght = 0;
	
	
	void Start () 
	{
		if(useHealthBar)
		{
			InitHealthBar();
		}
	}
	public void InitHealthBar()
	{
		if(!showOldGUI)
		{
			healthBar = GameObject.FindGameObjectWithTag("HealthBar");
			healthBar.SetActive(true);
			rectTrans = (RectTransform)healthBar.transform.GetChild(1);
			m_maxHealthBarLength = healthbarLenght;
		}
		else
		{
			m_maxGUIHealthBarLenght = GUIHealthBarLenght;
		}

		m_health = GetComponent<Health> ();
		m_transform = GetComponent<Transform> ();
	}
	public Vector3 GetRealPos(Vector3 pos)
	{
		Vector3 temp = Camera.main.WorldToScreenPoint (pos);
		return new Vector3 (temp.x, Screen.height - temp.y,temp.z);
	}
	public void AdjustHealthBar()
	{
		if(!showOldGUI)
		{
			if(m_health.health > 0)
				healthbarLenght = m_health.health / m_health.maxHealth * m_maxHealthBarLength;
			else
				healthbarLenght = 0;

			UpdateHealthBar ();
		}
		else
		{
			if(m_health.health > 0)
				GUIHealthBarLenght = m_health.health / m_health.maxHealth * m_maxGUIHealthBarLenght;
			else
				GUIHealthBarLenght = 0;
		}
	}
	void UpdateHealthBar()
	{
		rectTrans.sizeDelta = new Vector2 (healthbarLenght, rectTrans.sizeDelta.y);
	}
	void OnGUI()
	{
		if(useHealthBar && showOldGUI)
		{
			if(m_transform.GetChild(0).GetChild(0).renderer.isVisible)
			{
				GUI.DrawTexture(new Rect(GetRealPos(m_transform.position).x - hpBarOffsetX,GetRealPos(m_transform.position).y - hpBarOffsetY, m_maxGUIHealthBarLenght, 10), dmgTakenTexture);
				GUI.DrawTexture(new Rect(GetRealPos(m_transform.position).x - hpBarOffsetX,GetRealPos(m_transform.position).y - hpBarOffsetY, GUIHealthBarLenght, 10), healthBarTexture);
			}
		}
	}
}