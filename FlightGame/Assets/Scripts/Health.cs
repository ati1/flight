﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(NetworkView))]
public class Health : MonoBehaviour {

	public float health;
	public float maxHealth;
	public bool dead;
	public GameObject explosionFx;
	
	public bool isPlayer;

	private PlayGamesMultiplayer m_controller;
	private PlayerController m_player;

	private List<byte> m_damageMessage = new List<byte>();
	private List<byte> m_deathMessage = new List<byte>();
		
	void Awake()
	{
		health = maxHealth;

		if(isPlayer)
		{
			m_controller = FindObjectOfType<PlayGamesMultiplayer>();
			if(m_controller != null)
				m_player = GetComponent<PlayerController>();
		}
	}

	public void AdjustHealth(float healthChange)
	{
		float tempHealth = health + healthChange;
		if(tempHealth > 0)
			health += healthChange;
		else if(tempHealth <= 0 && !dead)
		{
			health = 0;
			Die ();
			MultiplayerDie();
		}
			
		if(GetComponent<HealthBar>())
			GetComponent<HealthBar> ().AdjustHealthBar ();

//		// if we are in multiplayer game and we lose health, print this to our log
//		if(m_controller != null && m_controller.GetMyId() == m_player.myParticipantId) 
//			m_controller.PrintMessage ("My hp " + health + "/" + maxHealth);
	}

	public void AdjustMultiplayerHealth(string id, float damage)
	{
		byte m_protocolVersion = 1;
		m_damageMessage.Clear ();
		m_damageMessage.Add (m_protocolVersion);
		m_damageMessage.Add ((byte)'H');

		m_damageMessage.AddRange (System.BitConverter.GetBytes (damage));
		//TODO: too slow figure out something better!!
		for( int i = 0;i < id.ToCharArray().Length; i++ )
		{
			m_damageMessage.Add((byte)id.ToCharArray()[i]);
		}

		byte[] messageToSend = m_damageMessage.ToArray();
		m_controller.SendMessageToEveryone(false, messageToSend);
	}

	public void Die()
	{
		if(GetComponent<PlayerController>())
		{
			StartCoroutine("Respawn");
		}
	}

	public void MultiplayerDie()
	{
		byte m_protocolVersion = 1;
		m_deathMessage.Clear ();
		m_deathMessage.Add (m_protocolVersion);
		m_deathMessage.Add ((byte)'D');

		byte[] messageToSend = m_deathMessage.ToArray();
		m_controller.SendMessageToEveryone(false, messageToSend);
	}
	
//	[RPC]
//	public void InstantiateFX(Vector3 pos)
//	{
//		//GameObject temp = GameObject.Instantiate (explosionFx, pos, Quaternion.identity) as GameObject;
//	}

	IEnumerator Respawn()
	{
		if(m_controller != null)
			transform.position = m_controller.GetRandomSpawnPoint ();
		else
			transform.position = FindObjectOfType<GameManager>().startPos.position;
		yield return new WaitForSeconds (0.1f);
		health = maxHealth;
		
		if(GetComponent<HealthBar>())
			GetComponent<HealthBar> ().AdjustHealthBar ();
	}
}