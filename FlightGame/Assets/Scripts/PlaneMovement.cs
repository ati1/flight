﻿using UnityEngine;
using System.Collections;

public class PlaneMovement : MonoBehaviour {

	public float speed;
	private float m_defSpeed;
	public float speedDecrease;
	public float verticalSpeed, horizontalSpeed;
	public bool allowMovement;
	[HideInInspector]
	public Camera playerCamera;
	private Transform m_transform;
	private Transform m_cameraTrans;
	
	private Gyroscope m_gyroScope;
	private Vector3 m_acceleration;
	private float m_x, m_z;

	public static Vector3 s_startPos;

	void Awake()
	{
		m_transform = transform;
		m_defSpeed = speed;

		if(Network.isClient || Network.isServer)
		{
			playerCamera = GetComponent<PlayerNetworkValues>().playerCamera.camera;
		}

		Vector3 startPos = new Vector3 (PlayerPrefs.GetFloat ("s_x"), 0, PlayerPrefs.GetFloat ("s_z"));
		//Debug.Log (startPos);
		s_startPos = startPos;

		if (s_startPos.sqrMagnitude > 1) 
		{
			s_startPos.Normalize();
		}
	}
	
	void FixedUpdate()
	{
		if(allowMovement)
			Move();
	}

	public void Move()
	{
		Vector3 cameraPos = m_transform.position - m_transform.forward * 15 + Vector3.up * 5;
		playerCamera.transform.position = playerCamera.transform.position * 0.96f + cameraPos * ( 1 - 0.96f );
		playerCamera.transform.LookAt (m_transform.position + m_transform.forward * 10);

		#if UNITY_STANDALONE_WIN
		m_transform.Rotate (Input.GetAxis ("Vertical") * verticalSpeed, 0, -Input.GetAxis ("Horizontal") * horizontalSpeed);
		#elif UNITY_ANDROID
		//m_transform.Rotate (-Input.acceleration.z * verticalSpeed,0,-Input.acceleration.x * horizontalSpeed);
		
//		m_gyroScope = Input.gyro;
//		m_gyroScope.enabled = true;
		
		m_acceleration = Input.acceleration;

		m_x = m_acceleration.x;
		m_z = m_acceleration.z;
		
		if(m_x > s_startPos.x && m_acceleration.x < s_startPos.x)
		{
			m_x = -Mathf.Abs (m_x);
		}
		if(m_x < s_startPos.x && m_acceleration.x > s_startPos.x)
		{
			m_x = Mathf.Abs (m_x);
		}	
		if(m_z > s_startPos.z && m_acceleration.z < s_startPos.z)
		{
			m_z = -Mathf.Abs (m_z);
		}
		if(m_z < s_startPos.z && m_acceleration.z > s_startPos.z)
		{
			m_z = Mathf.Abs (m_z);
		}
		
		if(m_acceleration.x > m_x && m_x > s_startPos.x || m_acceleration.x < m_x && m_x < s_startPos.z)
			m_x = m_acceleration.x;
		
		if(m_acceleration.z > m_z && m_z > s_startPos.z ||m_acceleration.z < m_z && m_z < s_startPos.z)
			m_z = m_acceleration.z;
	
		
		m_transform.Rotate ((-m_z - s_startPos.x) * verticalSpeed,0,(-m_x - s_startPos.z) * horizontalSpeed);
		
		#endif

		if(speed > m_defSpeed)
		{
			speed -= speedDecrease * Time.deltaTime;
		}
		m_transform.Translate (Vector3.forward * Time.deltaTime * speed);
	}
	
	public void IncreaseSpeed(float speedIncrease)
	{
		speed = speedIncrease;
	}
}