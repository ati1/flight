﻿using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour {

	public bool isShot;
	public GameObject scoreTextPrefab;
	private TargetShootingManager m_manager;
	
	void Awake()
	{
		m_manager = FindObjectOfType<TargetShootingManager>();
	}
	
	public void DoHit(int points)
	{
		if(!isShot)
		{
			isShot = true;
			m_manager.AddScore(points);
			GameObject temp = GameObject.Instantiate(scoreTextPrefab,transform.position,transform.rotation) as GameObject;
			temp.GetComponentInChildren<TextMesh>().text = "+"+points;
			gameObject.SetActive(false);
		}
	}
}