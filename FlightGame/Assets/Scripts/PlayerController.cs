﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi.Multiplayer;

public class PlayerController : MonoBehaviour {

	public string myParticipantId;
	public GameObject crosshair;
	
	private byte m_protocolVersion = 1;
	private List<byte> m_updateMessage = new List<byte>();
	
	private PlayGamesMultiplayer m_multiplayerManager;

	private Vector3 m_startPos;
	private Vector3 m_destinationPos;
	private Quaternion m_startRot;
	private Quaternion m_destinationRot;
	private float m_lastUpdateTime;

	private Vector3 m_lastKnownVel;


	void Awake()
	{
		m_multiplayerManager = FindObjectOfType<PlayGamesMultiplayer>();
		crosshair = transform.GetChild (1).gameObject;

		m_startPos = transform.position;
		m_startRot = transform.rotation;
	}
	
	
	public void SendMyUpdate(float posX, float posY, float posZ, float rotX, float rotY, float rotZ, float rotW) 
	{
		m_updateMessage.Clear ();
		m_updateMessage.Add (m_protocolVersion);
		m_updateMessage.Add ((byte)'U');
		
		m_updateMessage.AddRange (System.BitConverter.GetBytes (posX));
		m_updateMessage.AddRange (System.BitConverter.GetBytes (posY));  
		m_updateMessage.AddRange (System.BitConverter.GetBytes (posZ));
		m_updateMessage.AddRange (System.BitConverter.GetBytes (rotX));
		m_updateMessage.AddRange (System.BitConverter.GetBytes (rotY));
		m_updateMessage.AddRange (System.BitConverter.GetBytes (rotZ));
		m_updateMessage.AddRange (System.BitConverter.GetBytes (rotW));
		
		byte[] messageToSend = m_updateMessage.ToArray();
		m_multiplayerManager.SendMessageToEveryone(false, messageToSend);
	}

	public void ReceivePlaneUpdate(float posX, float posY, float posZ, float rotX, float rotY, float rotZ, float rotW) 
	{
		m_startPos = transform.position;
		m_startRot = transform.rotation;

		m_destinationPos = new Vector3 (posX, posY, posZ);
		m_destinationRot = new Quaternion(rotX,rotY,rotZ,rotW);

		m_lastKnownVel = transform.rotation * Vector3.forward * GetComponent<PlaneMovement> ().speed;

		m_lastUpdateTime = Time.time;
	}

	void FixedUpdate () 
	{
		if(m_multiplayerManager.GetMyId() != myParticipantId)
		{
			float messageSent = (Time.time - m_lastUpdateTime) / PlayGamesMultiplayer.s_timePerUpdate;
			
			if (messageSent <= 1.0f) 
			{
				transform.position = Vector3.Lerp (m_startPos, m_destinationPos, messageSent);
				transform.rotation = Quaternion.Slerp (m_startRot, m_destinationRot, messageSent);
			}
			else 
			{
				transform.position += (m_lastKnownVel * Time.deltaTime);
			}
		}
	}
}